#!/usr/bin/env python

import locale
import sys
import datetime
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

# France
locale.setlocale(locale.LC_ALL, 'fr_FR.utf8')

# Durées
year = datetime.timedelta(days=365)
month = datetime.timedelta(days=30.4)
day = datetime.timedelta(days=1)

today = datetime.date.today()

def diff_year(t, d):
    if t < d:
        return 0
    ans = t.year - d.year
    if t.month < d.month or (t.month == d.month and t.day < d.day):
        ans -= 1
    return ans

def diff_month(t, d):
    if t < d:
        return 0
    mois = (t.year - d.year) * 12 + t.month - d.month
    if t.day < d.day:
        mois -= 1
    return mois

def var_name(var):
    return [k for k, v in locals().iteritems() if v is var][0]

def find_depcom(dep, ville): # TODO improve efficiency
    import dbf
    T=dbf.Table('optimfiscale/misc/comsimp2016.dbf')
    T.open()
    depcom=None
    for t in T:
        if t.dep == dep+' '*(3-len(dep)) and t.ncc.find(ville.capitalize()):
            depcom = dep + t.com
            break
    print("le DEPCOM de ", ville, " est ", depcom)
    return depcom

def plot(t, d, titre='', clf=False, color='b', ls='-'):
    # Plot le solde au cours du temps
    if clf:
        # Efface
        plt.clf()
    if t[-1]-t[0] > 4 * year:
        plt.gca().xaxis.set_major_locator(mdates.YearLocator())  # annees
    else:
        plt.gca().xaxis.set_major_locator(mdates.MonthLocator()) # mois

    plt.plot(t, [0]*len(t), '--k')
    if isinstance(d, dict):
        for l, d in d.items():
            plt.plot(t, d, color=color, ls=ls, label=l)
    else:
        plt.plot(t, d, label=titre, color=color, ls=ls)
    plt.legend()
    plt.gcf().autofmt_xdate()
    plt.title('soldes au cours du temps [%s]' % titre)
   #plt.draw()
   #plt.pause(0.1)
   #plt.show(block=False)

class Logger(object):
    def __init__(self, name='log.txt'):
        self.terminal = sys.stdout
        self.log = open(name, "w")

    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)

    def flush(self):
        #this flush method is needed for python 3 compatibility.
        #this handles the flush command by doing nothing.
        #you might want to specify some extra behavior here.
        pass
