#!/usr/bin/env python

import datetime
from optimfiscale.finance import *
from optimfiscale.tools import *

class Bien(object):
    """
    Un bien matériel
    """
    def __init__(self,
                 valeur=0,
                 label='Bien',
                 date=today,
                 at=None,
                 charges=None):
        self.operations = []
        self.date = date
        self.date_vente = datetime.datetime.max.date()
        self.valeur = valeur
        self.label = label
        self.charges = charges
        # achat
        self.operations.append(Operation(
            valeur=-self.valeur,
            at=lambda t: t == self.date,
            label='Achat' + self.label))
        if self.charges:
            self.operations.append(Operation(
                valeur=-self.charges,
                at=lambda t: t.day == 1 and t >= self.date,
                label='Charges' + self.label))
        print("Achat du Bien [%s] le %s pour %6.2f €" %
              (self.label, self.date, self.valeur))

    # TODO ne pas prendre en compte l'achat ?
    def rapporte(self, annee=None, verbose=True):
        ''' rapporte = tout ce qui est positif sur une annee '''
        if not annee:
            annee = (today-year).year
        rapporte = sum([max(0, o.at(datetime.date(annee, 1, 1) + day*d, verbose=False))
                    for d in range(365)
                    for o in self.operations
                    if datetime.date(annee, 1, 1) + day*d > self.date])
        if verbose and rapporte > 0:
            print("Bien [%s] rapporte %6.2f € en %d" % (self.label, rapporte, annee))
        return rapporte

    def coute(self, annee=None):
        ''' coute = tout ce qui est negatif '''
        if not annee:
            annee = (today-year).year
        return sum([o.at(datetime.date(annee, 1, 1) + day*d)
                    for d in range(365)
                    for o in self.operations
                    if o.valeur < 0])

    def vente(self, valeur, date=today): # TODO plusvalue, ...
        self.date_vente = date
        self.operations.append(Operation(
            valeur=valeur,
            at=lambda t: t == date,
            label='Vente' + self.label
            ))

    def at(self, t, verbose=True):
        if t >= self.date and t < self.date_vente:
            return sum([o.at(t, verbose) for o in self.operations])
        elif t >= self.date_vente:
            return sum([o.at(t, verbose) for o in self.operations
                        if o.label[:5] == 'Vente'])
        return 0


class Immobilier(Bien):
    """
    Un bien immobilier en France avec charges, achat notaire, taxe foncière
    """
    def __init__(self,
            valeur=0,
            date=today,
            label='Immobilier',
            charges=None,
            taxe_fonciere=None,
            dep='75',
            ville='Paris',
            taux_notaire=0.08):
        super(Immobilier, self).__init__(
            valeur=valeur,
            label=label,
            date=date,
            charges=charges)
        self.locations_at = []
        self.loyers = []
        self.taxe_fonciere = taxe_fonciere
        self.dep = dep
        self.ville = ville
        self.taux_notaire = taux_notaire
        if self.taxe_fonciere: # payable le 1er novembre
            self.operations.append(Operation(
                valeur=-self.taxe_fonciere,
                at=lambda t: (t.day, t.month) == (1, 11) and t >= self.date,
                label='Taxe Foncière ' + self.label))

        # achat
        if self.taux_notaire != 0.:
            self.operations.append(Operation(
                valeur=-self.valeur * self.taux_notaire,
                at=lambda t: t == self.date,
                label='Notaire' + self.label))

    def louer(self, loyer=0, date=None, loue_at=None):
        "louer son bien"
        if date is None:
            date = self.date
        if loue_at is None:
            def loue_at(t): return (t.month, t.day) == (date.month, date.day)
        self.operations.append(Operation(
            valeur=loyer,
            at=loue_at,
            cat='Revenu',
            label='Location' + self.label))

    def rendement(self, methode='Brut'):
        if methode == 'Brut':
            return sum([l for l in self.loyers]) / self.coute()
        elif methode == 'Net':
            return 9./12. * sum([l for l in self.loyers]) / self.coute()
        else:
            return 9./12. * self.rapporte() / (self.valeur+self.coute())
