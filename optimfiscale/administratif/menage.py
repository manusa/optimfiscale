#!/usr/bin/env python

from optimfiscale.administratif.impot import *
from optimfiscale.administratif.bien import *
from optimfiscale.tools import *

class Menage(object):
    """
    Un Menage c'est parent(s) "pacse", "marie", "celibataire"
    et des enfants
    """
    def __init__(self,
                 parents=[],
                 enfants=[],
                 statut="celibataire",
                 SOL='proprietaire',
                 dep='75',
                 ville='Paris'):

        assert(statut in ["pacse", "marie", "celibataire"])
        assert(len(parents) in [1,2])
        self.parents = parents
        self.enfants = enfants
        self.statut = statut
        self.biens = [] # les biens immobiliers du menage
        self.emprunts = [] # les emprunts immobiliers du menage
        assert(SOL in [u"sans_domicile", u"primo_accedant", u"proprietaire", u"loge_gratuitement", u"locataire_vide",
            u"locataire_foyer", u"locataire_hlm"])
        self.SOL = SOL
        self.depcom = find_depcom(dep, ville)
        if not self.depcom:
            raise('Erreur: aucune ville '+ ville+ ' dans le departement ' + dep)
        self.taxes = TaxeFrance(self)

    def papa(self):
        return self.parents[0]

    def maman(self):
        if len(self.parents) == 2:
            return self.parents[1]
        else:
            return None

    def emprunt(self, c):
        self.emprunts.append(c)
        return c

    def achat(self, bien, SOL=None):
        self.biens.append(bien)
        if SOL and isinstance(bien, Immobilier):
            self.SOL = SOL
            self.taxes.menage = self
        return bien

    def vente(self, bien):
        self.biens.remove(bien)

    def update_at(self, t):
        for p in self.parents:
            p.update_at(t)
        for e in self.emprunts:
            e.update_at(t)
        if (t.day, t.month) == (1, 11): # TODO taxe le 1er novembre
            self.taxes.update_at(t)
            # TODO le 1er compte du papa paie les taxes
            self.papa().comptes[0].append(Operation(label='Impots', valeur = self.taxes.total(), at=lambda ta:ta==t))

    def __str__(self):
        return " ".join(self.parents) + " " + self.statut + " enfants: " + " ".join(self.enfants)
