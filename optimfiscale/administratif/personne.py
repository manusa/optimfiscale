#!/usr/bin/env python

import datetime
from optimfiscale.finance import *

class Personne(object):
    def __init__(self,
                 nom='Nom',
                 date_naissance=datetime.date(1980, 1, 1),
                 salaire=Salaire()):

        self.nom = nom
        self.date_naissance = date_naissance  # date de naissance
        self.comptes = []
        self.salaire = salaire

    def age(self, t, verbose=True):
        dn = self.date_naissance
        age = t.year - dn.year - int((t.month, t.day) < (dn.month, dn.day))
        if age < 0:
            return 0
        if verbose:
            print(self.nom, "a", age, "ans.")
        return age

    def __str__(self):
        return self.date_naissance.strftime("%d/%m/%y") + ' ' + self.nom + ':'+'\n'.join([':'.join([c.label, c.solde]) for c in self.comptes])

    def update_at(self, t):
        for c in self.comptes:
            c.solde_at(t)
