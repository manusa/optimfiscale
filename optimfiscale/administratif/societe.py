#!/usr/bin/env python

import datetime
from optimfiscale.finance import *

class Societe(Operation):
    # TODO
    def __init__(self, valeur=0, label='societe', date=(11, 1)):
        super(Societe, self).__init__(
            valeur=-base,
            at=lambda t: (t.day, t.month) == (date[1], date[0]),
            label=label
        )
        self.valeur = base
        self.date = date
        print("Societe [%s] percu lu le " % label, '/'.join(map(str, date)))


class SCI(Societe):
    def __init__(self, base=400, date=(10, 1)):
        super(SCI, self).__init__(
            base=base,
            label='SocieteImmobiliere',
            date=date)
