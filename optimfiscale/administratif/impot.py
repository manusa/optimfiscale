#!/usr/bin/env python

import json
import datetime
from optimfiscale.tools import *
from optimfiscale.finance.operation import *

from openfisca_core.simulations import Simulation
from openfisca_core.simulation_builder import SimulationBuilder
# Call module describing the French System
from openfisca_france import FranceTaxBenefitSystem

# Initialize the legislation
legislation_france = FranceTaxBenefitSystem()

"""
Openfisca taxes
"""
class TaxeFrance(object):
    """
    Classe de calcul d'impots par openfisca-france
    """
    def __init__(self, menage):
        self.menage = menage
        m = menage
        self.loyer = 0
        self.irpp = 0.
        self.alloc = 0.
        self.csg = 0.
        self.taxehab = 0.
        # statut_occupation_logement
        situation={
                'familles':{'famille_1': {'enfants':[]}},
                'foyers_fiscaux':{'foyer_fiscal_1':{'personnes_a_charge':[]}},
                'individus': {},
                'menages':{'menage_1':{'enfants':[]}}
                }
        situation['familles']['famille_1']['parents'] = [m.papa().nom]
        situation['foyers_fiscaux']['foyer_fiscal_1']['declarants'] = [m.papa().nom]
        situation['individus'][m.papa().nom] = {'salaire_de_base':{}, 'contrat_de_travail':{}}
        situation['individus'][m.papa().nom]['date_naissance'] = {'ETERNITY':m.papa().date_naissance.strftime('%F')}
        situation['menages']['menage_1']['personne_de_reference'] = [m.papa().nom]
        situation['menages']['menage_1']['statut_occupation_logement'] = {}
        situation['menages']['menage_1']['loyer'] = {}
        situation['menages']['menage_1']['depcom'] = {}

        if m.maman():
            situation['familles']['famille_1']['parents'].append(m.maman().nom)
            situation['foyers_fiscaux']['foyer_fiscal_1']['declarants'].append(m.maman().nom)
            situation['individus'][m.maman().nom] = {'salaire_de_base':{}, 'contrat_de_travail':{}}
            situation['individus'][m.maman().nom]['date_naissance'] = {'ETERNITY': m.maman().date_naissance.strftime('%F')}
            situation['individus'][m.maman().nom]['statut_marital'] = {}
            situation['menages']['menage_1']['conjoint'] = [m.maman().nom]
#           maman = dict(date_naissance=m.maman().date_naissance.year, salaire_de_base=m.maman().salaire.brut_annuel())

        for e in m.enfants:
            situation['familles']['famille_1']['enfants'].append(e.nom)
            situation['individus'][e.nom] = {'date_naissance': {'ETERNITY':e.date_naissance.strftime('%F')}}
            situation['menages']['menage_1']['enfants'].append(e.nom)
            #if e.age(t,False)<25:
            situation['foyers_fiscaux']['foyer_fiscal_1']['personnes_a_charge'].append(e.nom)

        self.situation = situation

    def total(self):
        return self.irpp + self.csg + self.alloc + self.taxehab

    def update_at(self, t, verbose=True):
        # se paye en novembre sur l annee N-1
        if (t.day, t.month) == (1,11):
            # some variables missing avant 2004
            N_1 = max(t.year-1, 2004)
            syear = '%s'%N_1
            m = self.menage
            self.situation['menages']['menage_1']['statut_occupation_logement'][syear] = m.SOL
            self.situation['menages']['menage_1']['depcom'][syear] = m.depcom
            self.situation['individus'][m.papa().nom]['contrat_de_travail'][syear] = 'temps_plein'
            self.situation['individus'][m.papa().nom]['salaire_de_base'][syear] = m.papa().salaire.brut_annuel() + sum([b.rapporte(N_1) for b in m.biens])
            if m.maman():
                self.situation['individus'][m.maman().nom]['statut_marital'][syear] = m.statut
                self.situation['individus'][m.maman().nom]['contrat_de_travail'][syear] = 'temps_partiel'
                self.situation['individus'][m.maman().nom]['salaire_de_base'][syear] = m.maman().salaire.brut_annuel()
            #self.situation['menages']['menage_1']['loyer'][syear] = self.loyer-sum([c.solde for c in m.emprunts if not c.rembourse()]),
            simulationBuilder = SimulationBuilder()
            simulation = simulationBuilder.build_from_dict(legislation_france, self.situation)
            self.irpp = simulation.calculate('irpp', syear)[0]
            # les alloc pour toute l'annee
            self.alloc = simulation.calculate_add('af', syear)[0]
            self.csg = 0.1*simulation.calculate('csg', syear)[0] # TODO
            self.taxehab = simulation.calculate('taxe_habitation', syear)[0]
            if verbose:
                print("[IRPP] ", self.irpp)
                print("[Alloc] ", self.alloc)
                print("[CSG] ", self.csg)
                print("[TaxeHab] ", self.taxehab)

# ' TODO remove
# Old classes
# '

class Impot(Operation):
    def __init__(self, base=0, taux=.1, label='arnaque', date=(11, 1)):
        super(Impot, self).__init__(
            valeur=-base*taux,
            at=lambda t: (t.day, t.month) == (date[1], date[0]),
            label=label
        )
        self.solde = base
        self.taux = taux
        self.date = date
        print("impot [%s] percu lu le " % label, '/'.join(map(str, date)))


class TaxeHabitation(Impot):
    def __init__(self, base=400, date=(10, 1)):
        super(TaxeHabitation, self).__init__(
            base=base,
            taux=1.,
            label='TaxeHabitation',
            date=date)


class IR(Impot):
    def __init__(self, base=0, taux=0.2, date=(11, 1)):
        super(IR, self).__init__(
            base=base,
            taux=taux,
            label='ImpotRevenu',
            date=date)

