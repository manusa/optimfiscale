#!/usr/bin/env python

"""
Martin a 2 enfants
Martin a deja une maison et 50000€
Martin achete plusieurs appt a credit et les loues
il a besoin de 1000 € par mois pour vivre
"""

import sys
from datetime import date as dtdate
from matplotlib import pyplot as plt
from optimfiscale.finance import *
from optimfiscale.administratif import *
from optimfiscale.tools import *
from optimfiscale.simulateur.simulate import simulate

def Martin(color='b', cout=sys.__stdout__):
    sys.stdout = cout
    date = dtdate(1999, 1, 1)
    Martin = Personne('Martin', date_naissance=dtdate(1950,3,1))
    # Martin a 2 enfants
    emilie = Personne('Emilie', date_naissance=dtdate(1984,6,18))
    lise = Personne('Lise', date_naissance=dtdate(1986,8,18))
    famille = Menage(parents=[Martin], enfants=[emilie, lise], dep='06', SOL='proprietaire', ville='Nice')
    # Martin achete plusieurs appt a credit et les loues
    # il a besoin de 1000 € par mois pour vivre
    compteCourant = Compte(solde=50000.)
    Martin.comptes.append(compteCourant)
    # Predica
    Martin.salaire = Salaire(net=1333, label='Predica')
    compteCourant.append(Martin.salaire)

    #credits immos
    immos = [
            Immobilier(valeur=22257., date=dtdate(1999,11, 30),label='Appt_Shakespeare', charges=70, dep='06', ville='Nice',taxe_fonciere=400),
            Immobilier(valeur=24000., date=dtdate(2000, 2, 24),label='Appt_5Californie', charges=70, dep='06', ville='Nice',taxe_fonciere=400),
            Immobilier(valeur=55000., date=dtdate(2004, 1, 1), label='Appt_3Californie', charges=80, dep='06', ville='Nice',taxe_fonciere=400),
            Immobilier(valeur=24391., date=dtdate(2004, 1, 1), label='Appt_Trachel21', charges=80, dep='06', ville='Nice',taxe_fonciere=500),
            Immobilier(valeur=90000., date=dtdate(2012, 1, 1),label='Appt_Trachel58', charges=100, dep='06', ville='Nice',taxe_fonciere=650)
            ]
    compteCourant.append(immos)

    # credit à 70% de la valeur du bien
    famille.emprunt(CreditImmobilier(compteCourant, 0.7*immos[0].valeur, date=immos[0].date, taux_annuel=0.060, duree=10))
    famille.emprunt(CreditImmobilier(compteCourant, 0.7*immos[1].valeur, date=immos[1].date, taux_annuel=0.054, duree=15))
    famille.emprunt(CreditImmobilier(compteCourant, 0.7*immos[2].valeur, date=immos[2].date, taux_annuel=0.054, duree=15))
    famille.emprunt(CreditImmobilier(compteCourant, 0.7*immos[3].valeur, date=immos[3].date, taux_annuel=0.054, duree=20))
    famille.emprunt(CreditImmobilier(compteCourant, 0.5*immos[4].valeur, date=immos[4].date, taux_annuel=0.046, duree=20))

    # besoins
    compteCourant.append(
        Operation(label='Besoins',
                  valeur = -1500.,
                  at=lambda t: t.day==14))

    # loue les appart + cher durant l'été
    immos[0].louer(loyer=1000, loue_at = lambda t: t.day==1 and (t.month == 7 or t.month == 8))
    immos[0].louer(loyer=530, loue_at = lambda t: t.day==1 and (t.month != 7 and t.month != 8))
 #  immos[0].vente(valeur=1.2*immos[0].valeur, date=dtdate(2018, 2, 2))
    immos[1].louer(loyer=800, loue_at = lambda t: t.day==1 and (t.month == 7 or t.month == 8))
    immos[1].louer(loyer=430, loue_at = lambda t: t.day==1 and (t.month != 7 and t.month != 8))
    immos[2].louer(loyer=1000, loue_at = lambda t: t.day==1 and (t.month == 7 or t.month == 8))
    immos[2].louer(loyer=530, loue_at = lambda t: t.day==1 and (t.month != 7 and t.month != 8))
    immos[3].louer(loyer=1000, loue_at = lambda t: t.day==1 and (t.month == 7 or t.month == 8))
    immos[3].louer(loyer=480, loue_at = lambda t: t.day==1 and (t.month != 7 and t.month != 8))
    immos[4].louer(loyer=1000, loue_at = lambda t: t.day==1 and (t.month == 7 or t.month == 8))
    immos[4].louer(loyer=480, loue_at = lambda t: t.day==1 and (t.month != 7 and t.month != 8))

    # impots
    compteCourant.append(TaxeHabitation(base=700))

    return simulate(famille,
            ti=date,
            tf=date+20*year,
            color=color
            )

if __name__ == '__main__':
    Martin('b')
    plt.show()
