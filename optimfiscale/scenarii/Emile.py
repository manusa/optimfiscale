#!/usr/bin/env python

"""
Emile a 1 enfant avec Vero
Emile achete un appt a credit pour y vivre
il a besoin de 1000 € par mois pour vivre
Emile a un salaire annuel brut de 50000€
"""

import sys
from datetime import date as dtdate
from matplotlib import pyplot as plt
from optimfiscale.finance import *
from optimfiscale.administratif import *
from optimfiscale.tools import *
from optimfiscale.simulateur.simulate import simulate

def Emile(color, cout=sys.__stdout__):
    sys.stdout = cout
    date = today
    Emile = Personne('Emile', date_naissance=dtdate(1984,6,1))
    Vero = Personne('Veronique', date_naissance=dtdate(1989,3,1))
    # Emile a 1 enfant avec Vero
    leo = Personne('Léo', date_naissance=dtdate(2018,11,1))
    famille = Menage(parents=[Emile, Vero], enfants=[leo])
    # Emile achete un appt a credit pour y vivre
    # il a besoin de 1000 € par mois pour vivre
    compteCourant = Compte(solde=15000.)
    Emile.comptes.append(compteCourant)

    immo = famille.achat(Immobilier(valeur=150000., date=date,label='Appt'), SOL='primo_accedant')
    compteCourant.append(immo)

    Emile.salaire = Salaire(brut_annuel=50000, label='Travail')
    compteCourant.append(Emile.salaire)
    # credit à 90% de la valeur du bien
    famille.emprunt(CreditImmobilier(compteCourant, 0.9*immo.valeur,
                                    date=immo.date, duree=15,
                                    taux_annuel=0.016,
                                    taux_assurance=0.004,
                                    garantie=1500))

    # besoins
    compteCourant.append(
        Operation(label='Besoins',
                  valeur = -1000.,
                  at=lambda t: t.day==14))

    # loue l'appart + cher durant l'été
    immo.louer(loyer=1000, loue_at = lambda t: t.day==1 and (t.month == 7 or t.month == 8))

    # impots
    compteCourant.append(TaxeHabitation(base=700))

    return simulate(famille,
            ti=date,
            tf=date+15*year,
            color=color
            )

if __name__ == '__main__':
    Emile('b')
    plt.show()
