#!/usr/bin/env python

"""
Michelle a 1 enfant avec Marc
Michelle loue un appt pour y vivre à 1100
elle a besoin de 1000 € par mois pour vivre
Michelle a un salaire annuel brut de 50000€
"""

import sys
from datetime import date as dtdate
from matplotlib import pyplot as plt
from optimfiscale.finance import *
from optimfiscale.administratif import *
from optimfiscale.tools import *
from optimfiscale.simulateur.simulate import simulate

def Michelle(color, cout=sys.__stdout__):
    sys.stdout = cout
    date = today
    Michelle = Personne('Michelle', date_naissance=dtdate(1984,6,1))
    Marc = Personne('Marc', date_naissance=dtdate(1989,3,1))
    leo = Personne('Léo', date_naissance=dtdate(2018,11,1))
    famille = Menage(parents=[Michelle, Marc], enfants=[leo], statut="marie", SOL='locataire_vide', dep='13', ville="marseille")

    compteCourant = Compte(solde=15000.)
    Michelle.comptes.append(compteCourant)

    appt = Operation(label="Loyer", valeur=-1100, at=lambda t: t.day==1)
    compteCourant.append(appt)
    Michelle.salaire = Salaire(brut_annuel=50000, label='Travail')
    compteCourant.append(Michelle.salaire)

    # besoins
    compteCourant.append(
        Operation(label='Besoins',
                  valeur = -1000.,
                  at=lambda t: t.day==14))

    # impots
    compteCourant.append(TaxeHabitation(base=700))

    return simulate(famille,
            ti=date,
            tf=date+20*year,
            color=color
            )

if __name__ == '__main__':
    Michelle('b')
    plt.show()
