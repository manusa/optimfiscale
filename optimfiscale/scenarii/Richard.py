#!/usr/bin/env python

"""
Richard achete un immeuble de rapport à 512k€ tout compris
credit 2400 € de credit + charges de 500€ et il rentre 3580€ rembourse sur 25 ans
il a besoin de 1500 € par mois pour vivre
Richard a un salaire de 60k€
"""

import sys
from datetime import date as dtdate
from matplotlib import pyplot as plt
from optimfiscale.finance import *
from optimfiscale.administratif import *
from optimfiscale.tools import *
from optimfiscale.simulateur.simulate import simulate

def Richard(color, cout=sys.__stdout__):
    date = today
    Richard = Personne('Richard', date_naissance=dtdate(1980,3,1))
    famille = Menage(parents=[Richard], dep='49', ville="Angers")
    # Richard achete un immeuble de rapport à 512k€ tout compris
    # credit 2400 € de credit et il rentre 3580€ rembourse sur 25 ans
    compteCourant = Compte(solde=50000.)
    Richard.comptes.append(compteCourant)

    # son salaire
    Richard.salaire = Salaire(brut_annuel=60000, label='Travail')
    compteCourant.append(Richard.salaire)

    # credit immo pour 512k€ dont le notaire
    immo = famille.achat(Immobilier(valeur=465000., date=date+4*day,label='Immeuble', charges=500))
    compteCourant.append(immo)

    # credit à 100% de la valeur du bien
    credit = famille.emprunt(CreditImmobilier(compteCourant, valeur=immo.valeur, taux_annuel=0.023, date=immo.date, duree=20, taux_assurance=0.005, garantie=2000))

    # besoins
    compteCourant.append(
        Operation(label='Besoins',
                  valeur = -1500.,
                  at=lambda t: t.day==14))
    compteCourant.append(
        Operation(label='Charges',
                  valeur = -500.,
                  at=lambda t: t.day==14))

    # il rentre 3580€ en loyer
    immo.louer(loyer=3580, loue_at = lambda t: t.day==1)

    # impots
    compteCourant.append(TaxeHabitation(base=1300))

    return simulate(famille,
            ti=date,
            tf=date+20*year,
            color=color
            )

if __name__ == '__main__':
    Richard('b')
    plt.show()
