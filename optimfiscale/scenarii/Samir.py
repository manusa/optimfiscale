#!/usr/bin/env python

"""
Samir a 1 enfant avec Vero
Samir achete une maison immense a credit pour la louer
il a besoin de 1000 € par mois pour vivre
Samir a pas de salaire
voir https://youtu.be/kZG496jRsuA
"""


import sys
import datetime
from datetime import date as dtdate
from matplotlib import pyplot as plt
from optimfiscale.finance import *
from optimfiscale.administratif import *
from optimfiscale.tools import *
from optimfiscale.simulateur.simulate import simulate

def Samir(color, cout=sys.__stdout__):
    sys.stdout = cout
    date = today
    Samir = Personne('Samir', date_naissance=dtdate(1984,6,1))
    famille = Menage(parents=[Samir], dep='49', ville="angers")
    # Samir achete une maison a credit pour la louer
    # il a besoin de 1000 € par mois pour vivre
    compteCourant = Compte(solde=10000.)
    Samir.comptes.append(compteCourant)

    # credit immo pour 346000 dont le notaire + travaux
    immo = famille.achat(Immobilier(valeur=350000., date=date+4*day,label='Villa'))

    compteCourant.append(immo)
    # credit à 100% de la valeur du bien + notaire
    famille.emprunt(CreditImmobilier(compteCourant, immo.valeur+28000, taux_annuel=0.015, taux_assurance=0.005, date=immo.date, duree=25))

    # besoins
    compteCourant.append(
        Operation(label='Besoins',
                  valeur = -1000.,
                  at=lambda t: t.day==14))

    # loue la maison en coloc 2*550+2*650 = 2400
    # puis dans 3 mois il louera les studios au fond du jardin
    # 650*2 = 3700
    def locationAVANT(t):
        return (t < (date + 3*month) and t.day==1)

    def locationAPRES(t):
        return (t >= (date + 3*month) and t.day==1)

    immo.louer(loyer=2400, loue_at = locationAVANT)
    immo.louer(loyer=3700, loue_at = locationAPRES)

    # impots
    compteCourant.append(TaxeHabitation(base=700))

    return simulate(famille,
            ti=date,
            tf=date+20*year,
            color=color
            )

if __name__ == '__main__':
    Samir('b')
    plt.show()
