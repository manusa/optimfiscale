#!/usr/bin/env python

# TODO
"""
Michel a 2 enfants avec Amelie
Ils créent une SCI qui achète une maison à crédit
pour une valeur de 300 000 €, ils remboursent à hauteur de 800€
par mois puis de 1400€ par mois 2 ans plus tard
Michel a un salaire annuel brut de 50000€
Amelie a un salaire net mensuel de 400€
"""

from optimfiscale import *

temps = today
Michel = Personne('Michel', ne=datetime.date(1984,6,1))
Amelie = Personne('Amelie', ne=datetime.date(1989,3,1))
# Michel a 2 enfants avec Amelie
leo = Personne('Léo', ne=datetime.date(2018,11,1))
lea = Personne('Léa', ne=datetime.date(2019,11,1))
famille = Menage(parents=[Michel, Amelie], enfants=[leo,lea], statut="marie")
# Ils créent une SCI qui achète une maison à crédit
# pour une valeur de 300 000 €, ils remboursent à hauteur de 800€
compteCourant = Compte(solde=500.)
compteCredit = Compte(solde=0.)
Michel.comptes.append(compteCourant)
Michel.comptes.append(compteCredit)

# Michel a un salaire annuel brut de 50000€
Michel.salaire = Salaire(brut_annuel=50000, label='Travail')
compteCourant.append(Michel.salaire)
# Amelie a un salaire net mensuel de 400€
Amelie.salaire = Salaire(net=400, label='Travail Danse Exotique')
compteCourant.append(Amelie.salaire)

sci = SCI([Michel, Amelie], valeur=10000)
sci.append(Credit(compteCourant, 300000, date=today+2*month, annees=30))

# besoins
compteCourant.append(
    Operation(label='Besoins',
              valeur = -600.,
              at=lambda t: t.day==14))

# impots
compteCourant.append(TaxeHabitation(base=500))
# TODO pour sci ?
taxes = TaxeFrance(famille, loyer=appt.valeur)

dates = [temps]
solde = [compteCourant.solde]
solde_credit = [compteCredit.solde_at(temps)]

while temps < today+15*year: # annees
    temps += day
    if temps.day == 1:
        print("------------------ %s:" % temps.strftime('%B %Y'))
        print("[%s] solde_crédit = %6.2f € solde = %6.2f €"%(temps,
                                                                  solde_credit[-1],
                                                                  solde[-1]))
        plot(t=dates, d=[(solde, 'solde'), (solde_credit, 'credit')], titre='Michel')

    # taxe le 1er novembre
    if (temps.day, temps.month) == (1,11):
        taxes.update_at(temps)
        compteCourant.solde += taxes.total()

    Michel.update_at(temps)
    solde.append(compteCourant.solde)
    solde_credit.append(compteCredit.solde)
    dates.append(temps)


plt.show()

