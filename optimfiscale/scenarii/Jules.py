#!/usr/bin/env python

"""
Jules achete un appt 48k€ 53k€ avec notaire, 110%
loyer 540€/mois credit 300€/mois dont assurance + 70 € de charges
LMNP loueur meuble non professionnel
2 ans apres T2 pres de la suisse loue 1000€/mois credit 230€ pendant un an puis 600€ 100€ de charges
achete 125k€ 135k€ avec notaire (au debut 132k€ il propose 115k€ elle dit 125k€)
3eme 45k€ + 10k€ de travaux (3 mois) finance à 110% loue à 580€ en meublé ou courte duree 50€/j
rembourse sur 25 ans encore 57k€ d'abord 90€ pendant 1 an puis 234€ en mensualite
il a besoin de 1000 € par mois pour vivre
Jules a un salaire de 400 net
"""

import sys
from datetime import date as dtdate
from matplotlib import pyplot as plt
from optimfiscale.finance import *
from optimfiscale.administratif import *
from optimfiscale.tools import *
from optimfiscale.simulateur.simulate import simulate

def Jules(color, cout=sys.__stdout__):
    sys.stdout = cout
    date = today
    Jules = Personne('Jules', date_naissance=dtdate(1984,6,1))
    famille = Menage(parents=[Jules], dep='31', ville="toulouse", SOL='loge_gratuitement')
    # Jules achete un premier appt
    # il a besoin de 1000 € par mois pour vivre
    compteCourant = Compte(solde=10000.)
    Jules.comptes.append(compteCourant)

    # credit immo pour 48000 dont le notaire + travaux
    immos = [
            famille.achat(Immobilier(valeur=48000.,
                                     date=date+4*day,label='ApptToulon',
                                     charges=70)),
            famille.achat(Immobilier(valeur=125000.,
                                     date=date+2*year,label='ApptFrontalier1',
                                     charges=100)),
            famille.achat(Immobilier(valeur=55000.,
                                     date=date+2*year,label='ApptFrontalier2',
                                     charges=70))
            ]
    compteCourant.append(immos)

    # credit à 110% de la valeur du bien
    famille.emprunt(CreditImmobilier(compteCourant, 1.1*immos[0].valeur, taux_annuel=0.012, date=immos[0].date, duree=15, taux_assurance=0.002))
    famille.emprunt(CreditImmobilier(compteCourant, 1.1*immos[1].valeur, taux_annuel=0.020, date=immos[1].date, duree=25, taux_assurance=0.002))
    famille.emprunt(CreditImmobilier(compteCourant, 1.1*immos[2].valeur, taux_annuel=0.013, date=immos[2].date, duree=20, taux_assurance=0.002))

    # Salaire
    Jules.salaire = Salaire(net=400, label='Travail')
    compteCourant.append(Jules.salaire)
    # besoins
    compteCourant.append(
        Operation(label='Besoins',
                  valeur = -1000.,
                  at=lambda t: t.day==14))

    # loyer 540€/mois credit 300€/mois dont assurance + 70 € de charges
    immos[0].louer(loyer=540, loue_at = lambda t: t.day==1)
    # loue 1000€/mois credit 230€ pendant un an puis 600€ 100€ de charges
    immos[1].louer(loyer=1000, loue_at = lambda t: t.day==1)
    immos[2].louer(loyer=570, loue_at = lambda t: t.day==1)

    # impots
    compteCourant.append(TaxeHabitation(base=700))

    return simulate(famille,
            ti=date,
            tf=date+25*year,
            color=color
            )

if __name__ == '__main__':
    Jules('b')
    plt.show()
