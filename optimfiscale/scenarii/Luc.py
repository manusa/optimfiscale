#!/usr/bin/env python

"""
Luc a 1 enfant avec Marcelle
Luc achete une maison à crédit pour y vivre et sous-loue un appt à 600€
il a besoin de 800 € par mois pour vivre
Luc a un salaire annuel brut de 50000€
Marcelle a un salaire net mensuel de 400€
"""

import sys
from datetime import date as dtdate
from matplotlib import pyplot as plt
from optimfiscale.tools import *
from optimfiscale.finance import *
from optimfiscale.administratif import *
from optimfiscale.simulateur.simulate import simulate

def Luc(color, cout=sys.__stdout__):
    sys.stdout = cout
    date = today
    Luc = Personne('Luc', date_naissance=dtdate(1984,6,1))
    Marcelle = Personne('Marcelle', date_naissance=dtdate(1989,3,1))
    # Luc a 1 enfant avec Marcelle
    leo = Personne('Léo', date_naissance=dtdate(2018,11,1))
    famille = Menage(parents=[Luc, Marcelle], enfants=[leo], statut="marie", dep='06', ville="Cannes")
    # Luc achete une maison pour y vivre et sous-loue un appt à 600
    # elle a besoin de 1000 € par mois pour vivre
    compteCourant = Compte(solde=53000.)
    Luc.comptes.append(compteCourant)

    # Luc a un salaire annuel brut de 50000€
    Luc.salaire = Salaire(brut_annuel=50000, label='Travail')
    compteCourant.append(Luc.salaire)
    # Marcelle a un salaire net mensuel de 400€
    Marcelle.salaire = Salaire(net=400, label='Travail Danse Exotique')
    compteCourantM = Compte(solde=3000.)
    compteCourantM.append(Marcelle.salaire)
    Marcelle.comptes.append(compteCourantM)

    # credit immo
    immo = famille.achat(Immobilier(valeur=400000., date=date+month,label='Maison'))
    compteCourant.append(immo)
    famille.emprunt(CreditImmobilier(compteCourant, 0.9*immo.valeur, date=immo.date, duree=20, taux_assurance=0.003, garantie=2000))

    # loue l'appart
    immo.louer(loyer=600, loue_at = lambda t: t.day==1)

    # besoins
    compteCourant.append(
        Operation(label='Besoins',
                  valeur = -800.,
                  at=lambda t: t.day==14))

    # impots
    compteCourant.append(TaxeHabitation(base=700))

    return simulate(famille,
            ti=date,
            tf=date+20*year,
            color=color
            )

if __name__ == '__main__':
    Luc('b')
    plt.show()
