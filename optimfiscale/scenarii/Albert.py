#!/usr/bin/env python

"""
Albert aura 2 enfants avec Judith
Albert loue une maison pour y vivre à 600€
il a besoin de 600 € par mois pour vivre
Albert a un salaire annuel brut de 30000€
Judith a un salaire net mensuel de 400€
"""

import sys
from datetime import date as dtdate
from matplotlib import pyplot as plt
from optimfiscale.finance import *
from optimfiscale.administratif import *
from optimfiscale.tools import *
from optimfiscale.simulateur.simulateur import simulate

def AlbertLoue(color, cout=sys.__stdout__):
    sys.stdout = cout
    date = today
    Albert = Personne('Albert', date_naissance=dtdate(1984,6,1))
    Judith = Personne('Judith', date_naissance=dtdate(1989,3,1))
    # Albert a 2 enfants avec Judith
    leo = Personne('Léo', date_naissance=dtdate(2018,11,1))
    lea = Personne('Léa', date_naissance=dtdate(2019,11,1))
    famille = Menage(parents=[Albert, Judith], enfants=[leo,lea], statut="marie", SOL='locataire_vide', dep='31', ville="toulouse")
    # Albert loue une maison pour y vivre à 600
    # elle a besoin de 600 € par mois pour vivre
    compteCourant = Compte(solde=15000.)
    Albert.comptes.append(compteCourant)

    # Albert a un salaire annuel brut de 30000€
    Albert.salaire = Salaire(brut_annuel=30000, label='Travail')
    compteCourant.append(Albert.salaire)
    # Judith a un salaire net mensuel de 400€
    Judith.salaire = Salaire(net=400, label='Travail Danse Exotique')
    compteCourant.append(Judith.salaire)
    appt = Operation(label="Loyer", valeur=-600, at=lambda t: t.day==1)
    compteCourant.append(appt)

    # besoins
    compteCourant.append(
        Operation(label='Besoins',
                  valeur = -600.,
                  at=lambda t: t.day==14))

    # impots
    compteCourant.append(TaxeHabitation(base=500))

    return simulate(famille,
            ti=date,
            tf=date+15*year,
            color=color
            )

"""
Albert aura 2 enfants avec Judith
Albert achete une maison pour y vivre à 120000€
il a besoin de 600 € par mois pour vivre
Albert a un salaire annuel brut de 30000€
Judith a un salaire net mensuel de 400€
"""
def AlbertAchete(color, cout=sys.__stdout__):
    sys.stdout = cout
    date = today
    Albert = Personne('Albert', date_naissance=dtdate(1984,6,1))
    Judith = Personne('Judith', date_naissance=dtdate(1989,3,1))
    # Albert a 2 enfants avec Judith
    leo = Personne('Léo', date_naissance=dtdate(2018,11,1))
    lea = Personne('Léa', date_naissance=dtdate(2019,11,1))
    famille = Menage(parents=[Albert, Judith], enfants=[leo,lea], statut="marie", dep='31', ville="toulouse")
    immo = famille.achat(Immobilier(valeur=120000.,
                                     date=today+4*day,label='Maison',
                                     charges=90))

    compteCourant = Compte(solde=15000.)
    compteCourant.append(immo)
    # credit à 110% de la valeur du bien
    famille.emprunt(CreditImmobilier(compteCourant, 1.1*immo.valeur,
                                   taux_annuel=0.015, date=immo.date, duree=20,
                                   taux_assurance=0.003, garantie=1500))
    Albert.comptes.append(compteCourant)

    # Albert a un salaire annuel brut de 30000€
    Albert.salaire = Salaire(brut_annuel=30000, label='Travail')
    compteCourant.append(Albert.salaire)
    # Judith a un salaire net mensuel de 400€
    Judith.salaire = Salaire(net=400, label='Travail Danse Exotique')
    compteCourant.append(Judith.salaire)

    # besoins
    compteCourant.append(
        Operation(label='Besoins',
                  valeur = -600.,
                  at=lambda t: t.day==14))

    # impots
    compteCourant.append(TaxeHabitation(base=500))

    return simulate(famille,
            ti=date,
            tf=date+20*year,
            color=color
            )

if __name__ == '__main__':
    AlbertAchete('b')
    plt.show()
