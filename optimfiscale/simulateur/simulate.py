#!/usr/bin/env python

"""
Simulateur de differentes personnes
"""

from optimfiscale.finance import *
from optimfiscale.tools import *
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

def simulate(menage, ti, tf, color='b'):

    dates = [ti]

    soldes = {}
    soldes['Total'] = [0.]
    for p in menage.parents:
        for c in p.comptes:
            soldes[p.nom+c.label] = [c.solde_at(ti)]
            soldes['Total'][-1] += soldes[p.nom+c.label][-1]

    for e in menage.emprunts:
        soldes[e.label] = [e.solde_at(ti)]
        soldes['Total'][-1] += soldes[e.label][-1]

    while ti < tf:
        ti += day
        dates.append(ti)
        if ti.day == 1: # tous les mois
            print("------------------")
            print("  {:%d-%m-%Y}".format(ti))
            print("------------------")
            for l, s in soldes.items():
                if l != "Total":
                    print('{:>40} = {: .3f} €'.format(l, s[-1]))
            print('{:>40} = {: .3f} €'.format("Total", soldes["Total"][-1]))

        menage.update_at(ti)

        soldes['Total'].append(0)
        for p in menage.parents:
            for c in p.comptes:
                soldes[p.nom+c.label].append(c.solde)
                soldes['Total'][-1] += c.solde

        for e in menage.emprunts:
            soldes[e.label].append(e.solde)
            soldes['Total'][-1] += e.solde

        # biens
        for b in menage.biens:
            if ti >= b.date:
                soldes['Total'][-1] += b.valeur

    # plots
    if tf-ti > 4 * year:
        plt.gca().xaxis.set_major_locator(mdates.YearLocator())  # annees
    else:
        plt.gca().xaxis.set_major_locator(mdates.MonthLocator()) # mois

    plt.plot(dates, [0]*len(dates), '--k')
    for p in menage.parents:
        for c in p.comptes:
            plt.plot(dates, soldes[p.nom+c.label], label=p.nom+'-'+c.label, color=color, ls='-')
    
    for e in menage.emprunts:
        plt.plot(dates, soldes[e.label], label = e.label, color=color, ls='--')

    plt.plot(dates, soldes['Total'], label='total', color=color, ls='-', marker='.', markevery=12)
    plt.legend()
    plt.gcf().autofmt_xdate()
    plt.title('soldes au cours du temps [%s]' % menage.papa().nom)

    plt.show()

    return dates, soldes, color
