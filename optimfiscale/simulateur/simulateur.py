#!/usr/bin/env python

"""
Simulateur de differentes personnes
"""

import sys
import os
from os import path as op
import pickle
import argparse
import numpy as np
from optimfiscale import finance, administratif, scenarii
from optimfiscale.scenarii import *

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Compute several investment scenarii.')
    parser.add_argument('-r', '--recompute', action='store_true', help='recompute all')

    args = parser.parse_args()
    np.random.seed(0) # to get reproducible colors
    python_version = sys.version[0:3]

    # choisir les scenarii à étudier
    simulations = [Barnabe]
    simulations = [AlbertAchete, Jules, Luc]

    for i, s in enumerate(simulations):
        color = np.random.rand(3,)
        # load already computed file if exists and no recomputation asked
        fname = op.join('save', python_version, s.__name__+'.bin')
        if not args.recompute and op.exists(fname):
            with open(fname, 'rb') as f:
                print("loading", fname)
                t, d, c = pickle.load(f)
                plot(t, d, titre=s.__name__, color=c)
        else:
            if not op.isdir(op.dirname(fname)):
                os.makedirs(op.dirname(fname))
            data = s(color=color, cout=Logger(fname.replace('.bin', '.log')))
            with open(fname, 'wb') as f:
                print("saving ", fname)
                pickle.dump(data, f)

