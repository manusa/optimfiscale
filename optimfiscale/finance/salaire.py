#!/usr/bin/env python

from optimfiscale.finance.operation import *

# TODO jour -> at pour que from ... ?
class Salaire(Operation):
    def __init__(self, brut_annuel=None, brut=None, net=None, at=None, label=''):
        if not at:
            def at(t): return (t.day == 1)
        if net:
            self.valeur = net
        elif brut:
            self.valeur = brut * 0.72  # approximation
        elif brut_annuel:
            self.valeur = brut_annuel / 17.  # approximation
        else:
            self.valeur = 0.
        super(Salaire, self).__init__(valeur=self.valeur,
                                      label='Salaire'+label,
                                      cat='Revenu',
                                      at=at)

    def brut_annuel(self):
        return self.valeur * 17
