#!/usr/bin/env python

import datetime
from optimfiscale.tools import *
from optimfiscale.finance.compte import *
from optimfiscale.tools import *
from string import ascii_uppercase

class Credit(Compte):
    Counter = 0
    """
    un Credit
    """
    def __init__(self, compte, valeur=0, taux_annuel=.015, taux_mensuel=None, duree=10, date=today, label=None):
        """
        compte à créditer/ponctioner
        valeur du credit, taux_annuel, duree, date, nom
        """
        # taux annuel <-> mensuel
        if taux_annuel and not taux_mensuel:
            taux_mensuel = taux_annuel/12. # proportionnelle
        while date.day > 28: # to avoid february singularity
            date += day
        if not label:
            label = 'Credit_'+ascii_uppercase[Credit.Counter%26]
            Credit.Counter += 1
        # mensualités constantes, juste la répartition intérêts change
        mensualite = valeur*taux_mensuel/(1.-(1.+taux_mensuel)**(-duree*12))
        self.cout = duree*12.*mensualite
        super(Credit, self).__init__(
                solde=0,
                label=label,
                )
        self.mensualite_interets = lambda t: taux_mensuel * self.solde
        # mensualite_capital = -mensualite - mensualite_interets(t)
        self.mensualite_capital = lambda t: max(-mensualite - self.mensualite_interets(t), self.solde)
        self.remboursement_capital = lambda t: - self.mensualite_capital(t)
        self.mensualite_at = lambda t, d=date: d < t < (d.replace(year=d.year+duree) + day) and (t.day == d.day) and not self.rembourse()
        # credite le compte du credit et des mensualites
        self.operations.append(Operation(label=' '.join(["Versement initial:", label]), valeur=-valeur, at=lambda t: (t == date)))
        compte.append(Operation(label=' '.join(["Versement initial:", label]), valeur=valeur, at=lambda t: (t == date)))
        compte.append(
                Operation(
                    label=' '.join(["Mensualité capital", label]),
                    valeur=self.mensualite_capital,
                    at=self.mensualite_at))
        compte.append(
                Operation(
                    label=' '.join(["Mensualité interêts", label]),
                    valeur=self.mensualite_interets,
                    at=self.mensualite_at,
                    cat='Interets'))
        self.operations.append(
                Operation(
            label=' '.join(["Remboursement", label]),
            valeur=self.remboursement_capital,
            at=self.mensualite_at))
        self.compte = compte
        self.valeur_init = valeur
        self.date = date
        self.duree = duree
        self.mensualite = mensualite
        self.taux_mensuel = taux_mensuel
        print(self)

    def remboursement_anticipe(self, d, valeur):
        # IRA - indemnite remboursement anticipé
        # min(6 mois d'interets au taux_annuel du pret,
        # 3% du capital restant) dû
        cout = lambda t: - valeur - min(0.03*abs(self.solde), abs(self.solde*self.taux_mensuel*6.))
        # si il n'y a pas assez sur le compte => return
        self.operations.append(
                Operation(
            label=' '.join(["Remboursement anticipé", self.label]),
            valeur=valeur,
            at=lambda t: (t==d)))
        self.compte.append(
            Operation(
                label='Remboursement anticipe + penalites',
                valeur=cout,
                at=lambda t: (t==d)))

    def rembourse(self):
        return (self.solde >= 0.)

    def update_at(self, t):
        if t > self.date and self.rembourse():
            return
        super(Credit, self).update_at(t)

    def __str__(self):
        return ("%s de %6.0f € à %2.2f %% contracté le %s pour %d ans. "
                "Mensualités à %.3f €. Coût total à %3.2f %% (%6.2f €)"
                % (self.label, self.valeur_init, self.taux_mensuel*1200, self.date,
                   self.duree, -self.mensualite, self.cout/self.valeur_init*100,
                   self.cout))

class CreditImmobilier(Credit):
    def __init__(self, compte, valeur=0, taux_annuel=.015, taux_mensuel=None, duree=10, date=today, label=None, taux_assurance=0, garantie=0):
        self.taux_assurance = taux_assurance
        self.garantie = garantie
        super(CreditImmobilier, self).__init__(compte=compte, valeur=valeur, taux_annuel=taux_annuel,  taux_mensuel=taux_mensuel, duree=duree, date=date, label=label)
        if garantie != 0:
            self.compte.append(Operation(valeur=-garantie, label=' '.join(["Garantie Logement:", self.label]), at=lambda t: (t == date)))
        if taux_assurance != 0:
            mensualite_assurance = lambda t, ta=self.taux_assurance: ta/12. * self.solde
            self.compte.append(Operation(
                    label=' '.join(["Mensualité assurance", self.label]),
                    valeur=mensualite_assurance,
                    at=self.mensualite_at))

    def remboursement_anticipe(self, t, valeur):
        super(CreditImmobilier, self).remboursement_anticipe(t, valeur)

    def __str__(self):
        return '\n'.join([super(CreditImmobilier, self).__str__(),
                "Assurance de prêt à %2.3f %%, et frais de garantie à %.2f €."%(self.taux_assurance*100, self.garantie)])
