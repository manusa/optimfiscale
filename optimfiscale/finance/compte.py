#!/usr/bin/env python

import datetime
from optimfiscale.tools import *
from optimfiscale.finance.operation import *

def quinzaines(t):
    "return le nombre de quinzaines jusqu'à la fin de l'année"
    q = 2*(12 - t.month)
    if (t.day < 16):
        q += 1
    return q

class Compte(object):
    def __init__(self, solde=0, taux_interets=0, label='Compte'):
        self.solde = solde
        self.taux_interets = taux_interets
        self.label = label
        # XXX approx: comme si ouvert au 1er janvier
        self.interets = taux_interets * solde
        self.date_interets = {'month': 12, 'day': 31}
        self.operations = []

    def append(self, other):
        """ append or extend """
        if isinstance(other, list):
            return self.operations.extend(other)
        elif isinstance(other, Operation):
            return self.operations.append(other)
        else:
            return self.operations.append(other)

    def __str__(self):
        return '%s: %f' % (self.label, self.solde)

    def update_at(self, t):
        """ update all operations at t """
        di = self.date_interets
        if (self.interets and (t.month, t.day) == (di['month'], di['day'])): # capitalisation des interets
            self.operations.append(
                    Operation(
                        self.interets, lambda t2:t2==datetime.date(t.year, di['month'], di['day']),
                        label='Interets' + self.label))

        sum = 0.
        for o in self.operations:
            v = o.at(t)
            if v:
                sum += v

        if sum!=0.:
            self.solde += sum
            self.interets += quinzaines(t)/24. * self.taux_interets * sum

    def solde_at(self, t):
        """ update and return the solde at t """
        self.update_at(t)
        return self.solde
