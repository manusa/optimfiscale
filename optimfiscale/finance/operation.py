#!/usr/bin/env python

from optimfiscale.tools import *

class Operation(object):
    """
    Classe de base pour toute operation financiere.
    """
    def __init__(self,
                 valeur=0,
                 at=lambda *args: True,
                 cat='',
                 label=''):

        self.date = today
        self.at_ = at
        self.valeur = valeur
        self.label = label
        self.cat = cat
        # self.type = type

    def at(self, t, verbose=True):
        "Defines the periodicity"
        if not self.at_(t):
            return 0.
        if isinstance(self.valeur, (int, float)):
            v = self.valeur
        else:
            v = self.valeur(t)
        if verbose:
            print('{:<40} {:<20} {:> 20.2f} €'.format(self.label, self.cat, v))
        return v

    def __str__(self):
        return ' '.join(self.report())

    def report(self):
       #return [self.date.strftime("%d/%m/%y"), self.label, self.cat, '%.2f'%self.valeur]
        return [self.label, self.cat, '%.2f'%self.valeur]
