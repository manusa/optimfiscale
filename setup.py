#!/usr/bin/env python

"""
@author: man
"""

import os

try:
    from setuptools import setup
    have_setuptools = True
except ImportError:
    from distutils.core import setup
    have_setuptools = False

def read(fname):
    with open(os.path.join(os.path.dirname(__file__), fname)) as f:
        return f.read()

stp = dict(
        name = "optimfiscale",
        version = "1.0.0",
        author = "Man Usashi",
        author_email = "manusashi@gmx.com",
        description = ("microsimulateur de credits et impots."),
        license = "GPL 3",
        keywords = ["fisc","money","argent","simulateur","taxe","python","banque"],
        url = "http://framagit.org/manusa/optimfiscale.git",
        packages=[
            'optimfiscale',
            'optimfiscale.finance',
            'optimfiscale.scenarii',
            'optimfiscale.administratif',
            'optimfiscale.simulateur',
            'optimfiscale.tools'],
        long_description=read('README.md'),
        classifiers=[
            "Development Status :: 0 - pre Alpha",
            "Topic :: Micro-simulation credit impots",
            "License :: OSI Approved :: GPL3 License",
            ],
        tests_require=['pytest'],
        data_files = [ ('optimfiscale/misc/comsimp2016.dbf', ['LICENSE', 'README.md']) ],
        extras_require = {
            'test': [
                'pytest',
                'flake8'
                ],
            },
        )

if have_setuptools:
    stp['install_requires'] = [
        'dbfread',
        'dbf',
        'openfisca-core >= 40.0.0',
        'OpenFisca-France >= 145.0.0',
        'pyyaml >= 3.10',
        'requests >= 2.8',
        'matplotlib >= 2.0'
    ]

setup(**stp)
