# Optimisastion fiscale:

[![pipeline status](https://framagit.org/manusa/optimfiscale/badges/master/pipeline.svg)](https://framagit.org/manusa/optimfiscale/commits/master) <-- does ot work yet

[![coverage report](https://framagit.org/manusa/optimfiscale/badges/master/coverage.svg)](https://framagit.org/manusa/optimfiscale/commits/master) <-- does ot work yet


> pip install -r requirements.txt
> python setup.py install

> `pytest`
ou
> `python optimfiscale/scenarii/Luc.py`
> `python optimfiscale/simulateur.py`

Le but étant d'étudier plusieurs scenari d'investissemens, voire le conseil
ou l'optimisation de ces opérations.

![screenshot](screenshot.png)

## Installation:

Installez un environnement virtuel avec [Pew](https://github.com/berdario/pew).

- Un _[virtualenv](https://virtualenv.pypa.io/en/stable/)_ crée un environnement pour les besoins spécifiques du projet sur lequel vous travaillez.
- Un gestionnaire de _virtualenv_, tel que [Pew](https://github.com/berdario/pew), vous permet de facilement créer, supprimer et naviguer entre différents projets.

Pour installer Pew, lancez une fenêtre de terminal et suivez ces instructions :

```sh
pip install --upgrade pip
pip install pew
```
Créez un nouveau _virtualenv_ nommé **optimfiscale** et configurez-le avec python3.7 :

```sh
pew new optimfiscale --python=python3.7
# Si demandé, répondez "Y" à la question sur la modification du fichier de configuration de votre shell
```
Le  _virtualenv_  **optimfiscale** sera alors activé, c'est-à-dire que les commandes suivantes s'exécuteront directement dans l'environnement virtuel. Vous verrez dans votre terminal :

```sh
Installing setuptools, pip, wheel...done.
Launching subshell in virtual environment. Type 'exit' or 'Ctrl+D' to return.
```

Informations complémentaires :
- sortez du _virtualenv_ en tapant `exit` (or Ctrl-D) ;
- re-rentrez en tapant `pew workon optimfiscale` dans votre terminal.

```sh
python setup.py install
```

### TODO:

- interets de retard à la banque, comptes PEL, etc...
- ameliorer/creer des menages pour calculs openfisca quels impots calculer...
- les impots à la bonne periode, alloc tout les mois
- taxes habitations, impots locaux, etc.. ? ou abonnements eau, EDF ?


### Depends on:

see requirements.txt
openfisca (http://openfisca.org/doc/key-concepts.html)


### DONE:

- associer avec openfisca
- associer les produits à des comptes
- associer des comptes à des personnes
- rendement locatif
- compte avec interets
- remboursement anticipé du crédit


### Dev:

- Une Operation peut s'associer à un Compte, un Compte à une Personne,
    une Personne à un Menage.
- toute Operation a une fonction `at` qui définit sa périodicité.
- tout cela doit être mise à jour chaque jour, afin d'integrer les changements
    sur les comptes dans le temps.
