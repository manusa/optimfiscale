#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
import pytest
import datetime
from optimfiscale.tools import *
from optimfiscale.finance.operation import *

def test_operation():
    o = Operation(1,lambda t:t.day==1, cat='A', label='salaire')
    t = datetime.date(2019,1,1)
    pay = 0
    while t < datetime.date(2019,1,1) + year:
      pay += o.at(t)
      t += day

    assert(pay==12)

def test_operation2():
    inflation = 0.1 # 10% !
    o = Operation(
            valeur = lambda t: -2.*(1.+inflation*diff_year(t, datetime.date(2019,1,1))),
            at = lambda t:t.day==1,
            cat='A',
            label='besoins')
    t = datetime.date(2019,1,1)
    pay = 0
    while t < datetime.date(2019,1,1) + 2 * year:
      pay += o.at(t)
      t += day

    assert abs(pay + 50.4) < 0.01
