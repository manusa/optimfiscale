#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
import pytest
from optimfiscale.tools import *
from optimfiscale.finance.compte import *
from optimfiscale.finance.salaire import *
from optimfiscale.finance.credit import *

def test_credit():
    # compte avec comme solde = interets
    c = Compte(solde=7750)
    t = datetime.date(2018,1,1)
    c1 = Credit(c, valeur=100000, taux_annuel = 0.015, duree=10, label='Credit', date=t)
    while t < datetime.date(2028,5,1):
      c.update_at(t)
      c1.update_at(t)
      t += day
      if t.day == 2:
          print("[%s] c.solde = %f"%(t.strftime("%d/%m/%y"), c.solde))
          print("[%s] c1.solde = %f"%(t.strftime("%d/%m/%y"), c1.solde))

    assert (abs(c.solde)<1.)
    assert (abs(c1.solde)<1.)

def test_credit2():
    # compte avec comme solde = interets
    c = Compte(solde=13616, taux_interets=0.00)
    t = datetime.date(2018,1,1)
    c1 = Credit(c, valeur=100000, taux_annuel = 0.013, duree=20, date=t)
    assert abs(c1.mensualite - 473.40) < 1.
    while t < datetime.date(2038,2,1):
      c.update_at(t)
      c1.update_at(t)
      t += day
      if t.day == 2:
          print("[%s] c.solde = %f"%(t.strftime("%d/%m/%y"), c.solde))

    assert (abs(c.solde)<1.)

def test_credit_immobilier():
    # compte avec comme solde = interets+garantie+assurance
    c = Compte(solde=1045+12+105.24, taux_interets=0.00)
    t = datetime.date(2018, 1, 1)
    c1 = CreditImmobilier(c, valeur=100000, taux_annuel = 0.01, duree=2, date=t, taux_assurance=0.001, garantie=12)
    assert abs(c1.mensualite - 4210.2) < .1
    while t < datetime.date(2020,2,1):
      c.update_at(t)
      c1.update_at(t)
      t += day
      if t.day == 2:
          print('%s'%(t.strftime("%d/%m/%y")), c, c1.solde)

    assert (abs(c.solde)<1.)

def test_credit_immobilier2():
    # compte avec comme solde = interets+garantie+assurance
    c = Compte(solde=83624.69, taux_interets=0.00)
    t = datetime.date(2018, 1, 1)
    c1 = CreditImmobilier(c, valeur=150000, taux_annuel = 0.004*12, duree=20, date=t, taux_assurance=0.00, garantie=0)
    assert abs(c1.mensualite - 973.44) < .1
    while t < datetime.date(2038,2,1):
      c.update_at(t)
      c1.update_at(t)
      t += day
      if (t.month, t.day) == (1,2):
          print('%s'%(t.strftime("%d/%m/%y")), c, c1.solde)

    assert (abs(c.solde)<1.)
