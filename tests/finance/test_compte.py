#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
import pytest
import datetime
from optimfiscale.tools import *
from optimfiscale.finance.operation import *
from optimfiscale.finance.compte import *

def test_compte():
    o1 = Operation(1,lambda t:t.day==1, cat='Revenu', label='salaire')
    o2 = Operation(-0.5,lambda t:t.day==2, cat='Loyer', label='loyer')
    c1 = Compte(solde=1000)
    c2 = Compte(solde=100)
    t = datetime.date(2019,2,1)
    c1.append(o1)
    c2.append(o2)
    while t < datetime.date(2019,2,1) + year:
      c1.update_at(t)
      c2.update_at(t)
      print("c1.solde = %d"%c1.solde)
      print("c2.solde = %d"%c2.solde)
      t += day

    assert(c1.solde + c2.solde==1106)

def test_compte2(): # avec 6% d'interets
    t = datetime.date(2019,2,1)
    c1 = Compte(solde=1000, taux_interets=0.06)
    o1 = Operation(100, lambda t:t==datetime.date(2019,8,25), label='versement')
    o2 = Operation(-100, lambda t:t==datetime.date(2019,9,29), label='retrait')
    c1.append(o1)
    c1.append(o2)
    while t < datetime.date(2019,2,1) + year:
      c1.update_at(t)
      print("c1.solde = %f interets = %f"%(c1.solde, c1.interets))
      t += day

    assert(c1.solde == 1060.5)
