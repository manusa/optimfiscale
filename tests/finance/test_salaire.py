#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
import pytest
import datetime
from optimfiscale.tools import basics as b
from optimfiscale.finance.compte import *
from optimfiscale.finance.salaire import *

def test_salaire():
    s1 = Salaire(brut=2000/0.72, at=lambda t:t.day == 2, label='Brut')
    s2 = Salaire(net=2000., label='Net')
    s3 = Salaire(brut_annuel=34000., label='BrutAnnuel')
    c = Compte(solde=1)
    t = datetime.date(2019,1,1)
    c.append([s1, s2, s3])
    while t < datetime.date(2019,1,1) + b.year:
      c.update_at(t)
      print("c.solde = %d"%c.solde)
      t += b.day

    assert(c.solde==2000*12*3+1)
