#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
import pytest
import datetime
from optimfiscale.tools import *
from optimfiscale.finance.compte import *
from optimfiscale.finance.salaire import *
from optimfiscale.administratif.impot import *

def test_impot():
    # TODO
    s1 = Salaire(brut=2000/0.72, label='Brut')
    s2 = Salaire(net=2000., label='Net')
    s3 = Salaire(brut_annuel=34000., label='BrutAnnuel')
    c = Compte(solde=1)
    t = datetime.date(2019,2,1)
    c.append([s1, s2, s3])
    while t < datetime.date(2019,2,1) + year:
      c.update_at(t)
      print("c.solde = %d"%c.solde)
      t += day

    assert(c.solde==2000*12*3+1)
