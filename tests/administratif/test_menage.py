#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
import pytest
import datetime
from optimfiscale.tools import *
from optimfiscale.finance.compte import *
from optimfiscale.finance.salaire import *
from optimfiscale.administratif.personne import *
from optimfiscale.administratif.menage import *

def test_menage():
    s = Salaire(net=2000., label='Net')
    c = Compte(solde=1)
    h = Personne(nom="Jean",
            date_naissance=datetime.date(1980,1,1),
            salaire=s)
    f = Personne(nom="Jeanne",
            date_naissance=datetime.date(1980,1,1))
    e1 = Personne(nom="Jeannot",
            date_naissance=datetime.date(2000,1,1))
    e2 = Personne(nom="Luc",
            date_naissance=datetime.date(2002,1,1))
    h.comptes.append(c)
    t = datetime.date(2020,2,2)
    c.append([s])
    m = Menage([h,f], [e1,e2], statut="marie", dep='13', ville='Marseille')
    assert(m.papa().age(t) == 40)
    assert(m.maman().age(t) == 40)
