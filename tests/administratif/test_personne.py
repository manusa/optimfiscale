#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
import pytest
import datetime
from optimfiscale.tools import *
from optimfiscale.finance.compte import *
from optimfiscale.finance.salaire import *
from optimfiscale.administratif.personne import *

def test_personne():
    s = Salaire(net=2000., label='Net')
    c = Compte(solde=1)
    p = Personne(nom="Jean",
            date_naissance=datetime.date(1980,1,1),
            salaire=s)
    p.comptes.append(c)
    t = datetime.date(2020,2,2)
    c.append([s])
    assert(p.age(t) == 40)
