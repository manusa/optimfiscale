#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
import pytest
import datetime
from optimfiscale.tools import *
from optimfiscale.finance.compte import *
from optimfiscale.finance.salaire import *
from optimfiscale.administratif.personne import *
from optimfiscale.administratif.menage import *
from optimfiscale.administratif.bien import *

def test_bien():
    c = Compte(solde=1012)
    h = Personne(nom="Jean",
            date_naissance=datetime.date(1980,1,1))
    h.comptes.append(c)
    now = datetime.date(2018,1,1)
    voiture = Bien(valeur=1000., date=now+2*day,label='Voiture', charges=1.)
    c.append(voiture)
#   immo = Immobilier(valeur=155000., date=now+4*day,label='Appt', charges=75., taxe_fonciere=700)
#   c.append(immo)
    t = now
    while t < datetime.date(2019,2,1):
      c.update_at(t)
      print("[%s] c.solde = %f"%(t.strftime("%d/%m/%y"), c.solde))
      t += day
    assert c.solde == 0

def test_bien2():
    c = Compte(solde=112000)
    h = Personne(nom="Jean",
            date_naissance=datetime.date(1980,1,1))
    h.comptes.append(c)
    immo = Immobilier(valeur=100000., date=datetime.date(2018,1,4), label='Appt', charges=100., taxe_fonciere=800, taux_notaire=0.1)
    c.append(immo)
    t = datetime.date(2018,1,1)
    while t < datetime.date(2019,2,1):
      c.update_at(t)
      print("[%s] c.solde = %f"%(t.strftime("%d/%m/%y"), c.solde))
      t += day
    assert c.solde == 0
